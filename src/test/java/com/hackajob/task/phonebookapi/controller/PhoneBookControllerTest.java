package com.hackajob.task.phonebookapi.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.hackajob.task.phonebookapi.PhonebookApiApplication;
import com.hackajob.task.phonebookapi.entity.Directory;
import com.hackajob.task.phonebookapi.repository.DirectoryRepository;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { PhonebookApiApplication.class })
@WebAppConfiguration
@AutoConfigureMockMvc
public class PhoneBookControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private DirectoryRepository directoryRepository;

	private String testToken = null;
	Directory directory = null;
	Optional<Directory> optDirectory = null;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		generateToken("abc@gmail.com");
		directory = new Directory();
		directory.setDirectoryId(new Long(1));
		directory.setAlternateNumber("1234");
		directory.setEmail("test@gmail.com");
		directory.setUserName("test-user");
		directory.setMobileNumber("23566");
		directory.setUserName("test-user");
		optDirectory = Optional.of(directory);
	}

	private void generateToken(String email) {

		Algorithm algorithm = Algorithm.HMAC256("secret");
		testToken = "Bearer " + JWT.create().withSubject(email).withIssuer("phonebook").withClaim("role", "user")
				.withExpiresAt(new Date(System.currentTimeMillis() + 5 * 60 * 1000)).sign(algorithm);
	}

	@Test
	public void testAddEntry() throws Exception {

		when(directoryRepository.save(any())).thenReturn(directory);

		mockMvc.perform(post("/phonebook/").contentType(MediaType.APPLICATION_JSON_VALUE)
				.content("{\"userName\":\"Libin Thomas\",\"mobileNumber\":\"9875678\","
						+ "\"alternateMobileNumber\":\"78787878\",\"email\":\"abc@gmail.com\"}")
				.header("Authorization", testToken)).andDo(MockMvcResultHandlers.print())
				.andExpect(jsonPath("$.code").value("201 CREATED"))
				.andExpect(jsonPath("$.response.directoryId").value(1));
	}

	@Test
	public void testAddEntryInvalidAuthToken() throws Exception {

		testToken = "Bearer jhgjgjhgj";
		mockMvc.perform(post("/phonebook/").contentType(MediaType.APPLICATION_JSON_VALUE)
				.content("{\"userName\":\"Libin Thomas\",\"mobileNumber\":\"9875678\","
						+ "\"alternateMobileNumber\":\"78787878\",\"email\":\"abc@gmail.com\"}")
				.header("Authorization", testToken)).andDo(MockMvcResultHandlers.print())
				.andExpect(jsonPath("$.code").value("ValidationException"))
				.andExpect(jsonPath("$.status").value("Invalid Authorization Token"));

	}

	@Test
	public void testUpdateEntry() throws Exception {
		when(directoryRepository.findById(Mockito.anyLong())).thenReturn(optDirectory);
		mockMvc.perform(put("/phonebook/1").contentType(MediaType.APPLICATION_JSON_VALUE)
				.content("{\"email\":\"test-updated@gmail.com\"}").header("Authorization", testToken))
				.andDo(MockMvcResultHandlers.print()).andExpect(jsonPath("$.code").value("200 OK"));
	}

	@Test
	public void testDeleteEntry() throws Exception {

		when(directoryRepository.findById(Mockito.anyLong())).thenReturn(optDirectory);

		mockMvc.perform(delete("/phonebook/id/1").header("Authorization", testToken)).andDo(MockMvcResultHandlers.print())
				.andExpect(jsonPath("$.code").value("200 OK"));
	}

	@Test
	public void testGetAllEntries() throws Exception {
		when(directoryRepository.findAll()).thenReturn(new ArrayList<>(Arrays.asList(directory)));

		mockMvc.perform(get("/phonebook/").header("Authorization", testToken)).andDo(MockMvcResultHandlers.print())
				.andExpect(jsonPath("$.code").value("200 OK"))
				.andExpect(jsonPath("$.response[0].userName").value("test-user"));
	}

	@Test
	public void testGetEntry() throws Exception {
		when(directoryRepository.findById(Mockito.anyLong())).thenReturn(optDirectory);

		mockMvc.perform(get("/phonebook/id/1").header("Authorization", testToken)).andDo(MockMvcResultHandlers.print())
				.andExpect(jsonPath("$.code").value("200 OK"))
				.andExpect(jsonPath("$.response.userName").value("test-user"));
	}

}
