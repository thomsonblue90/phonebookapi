package com.hackajob.task.phonebookapi.manager;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.hackajob.task.phonebookapi.dao.DirectoryDAO;
import com.hackajob.task.phonebookapi.entity.Directory;
import com.hackajob.task.phonebookapi.exception.EntryNotFoundException;
import com.hackajob.task.phonebookapi.model.request.DirectoryDTO;

public class DirectoryManagerTest {

	@Mock
	private DirectoryDAO directoryDAO;

	@InjectMocks
	private DirectoryManager directoryManager;

	Directory directory = null;
	Optional<Directory> optDirectory = null;
	DirectoryDTO directoryInput = null;

	@Before
	public void initMocks() {
		MockitoAnnotations.initMocks(this);
		directory = new Directory();
		directory.setDirectoryId(new Long(1));
		directory.setAlternateNumber("1234");
		directory.setEmail("test@gmail.com");
		directory.setUserName("test-user");
		directory.setMobileNumber("23566");
		directory.setUserName("test-user");
		directory.setDirectoryId(new Long(1));
		optDirectory = Optional.of(directory);

		directoryInput = new DirectoryDTO();
		directoryInput.setAlternateNumber("1234");
		directoryInput.setEmail("test@gmail.com");
		directoryInput.setUserName("test-user");
		directoryInput.setMobileNumber("23566");
	}

	@Test
	public void testAddEntry() {

		when(directoryDAO.addEntry(Mockito.any())).thenReturn(directory);
		Long id = directoryManager.addEntry(directoryInput);
		assertEquals(1, id.longValue());

	}

	@Test(expected = EntryNotFoundException.class)
	public void testUpdateEntry() {

		directoryManager.updateEntry(1, directoryInput);

	}

	@Test(expected = EntryNotFoundException.class)
	public void testDeleteEntry() {

		directoryManager.deleteEntry(1);

	}

	@Test
	public void testFindDirectoryById() {

		when(directoryDAO.findDirectoryById(Mockito.anyLong())).thenReturn(optDirectory);
		DirectoryDTO directoryDTO = directoryManager.findDirectoryById(1);
		assertNotNull(directoryDTO);
		assertEquals("test-user", directoryDTO.getUserName());

	}

	@Test
	public void testFindAllEntries() {

		when(directoryDAO.findAll()).thenReturn(new ArrayList<>(Arrays.asList(directory)));
		List<DirectoryDTO> directoryList = directoryManager.findAllEntries();
		assertNotNull(directoryList);
		assertEquals(1, directoryList.size());

	}

}
