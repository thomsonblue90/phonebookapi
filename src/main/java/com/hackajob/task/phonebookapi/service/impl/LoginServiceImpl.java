package com.hackajob.task.phonebookapi.service.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.hackajob.task.phonebookapi.exception.RequestIncompleteException;
import com.hackajob.task.phonebookapi.manager.LoginManager;
import com.hackajob.task.phonebookapi.model.request.UserDTO;
import com.hackajob.task.phonebookapi.service.LoginService;

@Component
public class LoginServiceImpl implements LoginService {

	@Autowired
	private LoginManager loginManager;

	@Override
	public void registerUser(UserDTO user) {

		if (null != user.getEmail() && null != user.getFirstName() && null != user.getLastName()
				&& null != user.getPassword()) {
			loginManager.registerUser(user);
		} else {
			throw new RequestIncompleteException(
					"Mandatory request parameters are not present - firstName, lastName, email, password");
		}

	}

	@Override
	public String loginUser(UserDTO user) {

		String token = null;
		if (null != user.getEmail() && null != user.getPassword()) {
			if (loginManager.authenticateUser(user)) {
				Algorithm algorithm = Algorithm.HMAC256("secret");
				token = JWT.create().withSubject(user.getEmail()).withIssuer("phonebook").withClaim("role", "user")
						.withExpiresAt(new Date(System.currentTimeMillis() + 5 * 60 * 1000)).sign(algorithm);
			}
		} else {
			throw new RequestIncompleteException("Mandatory request parameters are not present - email, password");
		}
		return token;
	}

}
