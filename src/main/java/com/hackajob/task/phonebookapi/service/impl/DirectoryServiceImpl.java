package com.hackajob.task.phonebookapi.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.hackajob.task.phonebookapi.exception.RequestIncompleteException;
import com.hackajob.task.phonebookapi.manager.DirectoryManager;
import com.hackajob.task.phonebookapi.model.request.DirectoryDTO;
import com.hackajob.task.phonebookapi.service.DirectoryService;

@Component
public class DirectoryServiceImpl implements DirectoryService {

	@Autowired
	private DirectoryManager directoryManager;

	@Override
	public long addEntry(DirectoryDTO directory) {

		if (null != directory.getUserName() && null != directory.getEmail() && null != directory.getMobileNumber()) {

			Long directoryId = directoryManager.addEntry(directory);

			return directoryId.longValue();
		} else {
			throw new RequestIncompleteException(
					"Mandatory request parameters are not present - userName, email, mobileNumber");
		}
	}

	@Override
	public void updateEntry(int id, DirectoryDTO directory) {

		if (null == directory.getAlternateNumber() && null == directory.getMobileNumber()
				&& null == directory.getEmail()) {
			throw new RequestIncompleteException(
					"Atleast one non-empty parameter is expected - alternateNumber, email, mobileNumber");
		} else {
			directoryManager.updateEntry(id, directory);
		}

	}

	@Override
	public DirectoryDTO getEntryById(int id) {

		return directoryManager.findDirectoryById(id);
	}
	
	@Override
	public DirectoryDTO getEntryByEmail(String email) {

		return directoryManager.findDirectoryByEmail(email);
	}

	@Override
	public List<DirectoryDTO> getALlEntries() {
		return directoryManager.findAllEntries();
	}

	@Override
	public void deleteEntryById(int id) {

		directoryManager.deleteEntry(id);
	}
	
	@Override
	public void deleteEntryByEmail(String email) {

		directoryManager.deleteEntryByEmail(email);
	}

}
