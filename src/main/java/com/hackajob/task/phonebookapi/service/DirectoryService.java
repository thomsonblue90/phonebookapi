package com.hackajob.task.phonebookapi.service;

import java.util.List;

import com.hackajob.task.phonebookapi.model.request.DirectoryDTO;

public interface DirectoryService {

	public long addEntry(DirectoryDTO directory);

	public void updateEntry(int id, DirectoryDTO directory);

	public DirectoryDTO getEntryById(int id);

	public List<DirectoryDTO> getALlEntries();

	public void deleteEntryById(int id);
	
	public void deleteEntryByEmail(String email);
	
	public DirectoryDTO getEntryByEmail(String email);

}
