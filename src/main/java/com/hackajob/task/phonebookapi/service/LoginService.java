package com.hackajob.task.phonebookapi.service;

import com.hackajob.task.phonebookapi.model.request.UserDTO;

public interface LoginService {

	public void registerUser(UserDTO user);

	public String loginUser(UserDTO user);
}
