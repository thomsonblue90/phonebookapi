package com.hackajob.task.phonebookapi.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.CreationTimestamp;

@Entity
public class Directory {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long directoryId;
	private String userName;
	private String mobileNumber;
	private String alternateNumber;
	private String email;
	
	@CreationTimestamp
	private Date created;
	
	public Long getDirectoryId() {
		return directoryId;
	}
	public void setDirectoryId(Long directoryId) {
		this.directoryId = directoryId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getAlternateNumber() {
		return alternateNumber;
	}
	public void setAlternateNumber(String alternateNumber) {
		this.alternateNumber = alternateNumber;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}
}
