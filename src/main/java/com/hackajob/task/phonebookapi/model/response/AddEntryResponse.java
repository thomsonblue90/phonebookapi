package com.hackajob.task.phonebookapi.model.response;

public class AddEntryResponse {

	private long directoryId;

	public long getDirectoryId() {
		return directoryId;
	}

	public void setDirectoryId(long directoryId) {
		this.directoryId = directoryId;
	}
	
	
}
