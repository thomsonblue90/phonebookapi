package com.hackajob.task.phonebookapi.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.hackajob.task.phonebookapi.dao.LoginDAO;
import com.hackajob.task.phonebookapi.entity.RegisteredUser;
import com.hackajob.task.phonebookapi.repository.UserRepository;

@Component
public class LoginDAOImpl implements LoginDAO {

	@Autowired
	private UserRepository userRepository;

	@Override
	public void registerUser(RegisteredUser user) {

		userRepository.save(user);
	}

	@Override
	public RegisteredUser findUser(String email) {
		RegisteredUser user = userRepository.findByEmail(email);
		return user;
	}

}
