package com.hackajob.task.phonebookapi.dao;

import java.util.Optional;

import com.hackajob.task.phonebookapi.entity.Directory;

public interface DirectoryDAO {

	public Directory addEntry(Directory entity);

	public Optional<Directory> findDirectoryById(Long id);
	
	public Optional<Directory> findDirectoryByEmail(String email);

	public void deleteDirectory(Directory entity);

	public Iterable<Directory> findAll();

}
