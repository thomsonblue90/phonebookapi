package com.hackajob.task.phonebookapi.dao.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.hackajob.task.phonebookapi.dao.DirectoryDAO;
import com.hackajob.task.phonebookapi.entity.Directory;
import com.hackajob.task.phonebookapi.repository.DirectoryRepository;

@Component
public class DirectoryDAOImpl implements DirectoryDAO {

	@Autowired
	private DirectoryRepository directoryRepository;

	@Override
	public Directory addEntry(Directory entity) {
		return directoryRepository.save(entity);
	}

	@Override
	public Optional<Directory> findDirectoryById(Long id) {
		return directoryRepository.findById(id);
	}
	
	@Override
	public Optional<Directory> findDirectoryByEmail(String email) {
		return directoryRepository.findByEmail(email);
	}

	@Override
	public void deleteDirectory(Directory entity) {

		directoryRepository.delete(entity);
	}

	@Override
	public Iterable<Directory> findAll() {
		return directoryRepository.findAll();
	}

}
