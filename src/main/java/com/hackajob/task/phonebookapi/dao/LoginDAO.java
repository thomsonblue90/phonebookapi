package com.hackajob.task.phonebookapi.dao;

import com.hackajob.task.phonebookapi.entity.RegisteredUser;

public interface LoginDAO {

	public void registerUser(RegisteredUser userEntity);
	
	public RegisteredUser findUser(String email);
}
