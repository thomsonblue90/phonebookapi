package com.hackajob.task.phonebookapi.exception;

public class EntryNotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2860189315786989181L;

	private final String message;

	public EntryNotFoundException(String message) {
		this.message = message;
	}

	@Override
	public String getMessage() {
		return message;
	}
}
