package com.hackajob.task.phonebookapi.exception;

public class RequestIncompleteException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5541133676403525858L;

	private final String message;

	public RequestIncompleteException(String message) {
		this.message = message;
	}

	@Override
	public String getMessage() {
		return message;
	}
}
