package com.hackajob.task.phonebookapi.exception;

public class ValidationException extends RuntimeException {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2461526822967365882L;
	
	private final String message;

	public ValidationException(String message) {
		this.message = message;
	}

	@Override
	public String getMessage() {
		return message;
	}
}
