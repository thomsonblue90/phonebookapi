package com.hackajob.task.phonebookapi.exception;

public class AuthenticationFailureException extends RuntimeException {


	/**
	 * 
	 */
	private static final long serialVersionUID = 3974415482631016275L;
	private final String message;

	public AuthenticationFailureException(String message) {
		this.message = message;
	}

	@Override
	public String getMessage() {
		return message;
	}
}
