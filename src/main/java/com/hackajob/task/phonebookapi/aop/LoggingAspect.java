package com.hackajob.task.phonebookapi.aop;

import java.util.Arrays;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class LoggingAspect {

	private static final Logger LOGGER = LoggerFactory.getLogger(LoggingAspect.class);

	@Pointcut("execution(* com.hackajob.task.phonebookapi.controller.*.*(..))")
	public void pointCutMethods() {
		// no method body is required as the method is to define the pointcuts in
		// annotation
	}

	@Around("pointCutMethods()")
	public Object profileMethods(ProceedingJoinPoint pjp) throws Throwable {

		long start = System.currentTimeMillis();
		Object output = null;

		LOGGER.info("{}", pjp.getTarget().getClass());
		LOGGER.info("entry -> method -> {} ", pjp.getSignature().getName());

		try {

			output = pjp.proceed();
			long elapsedTime = System.currentTimeMillis() - start;
			LOGGER.info("Method execution time in ms-> {} ", elapsedTime);
			LOGGER.info("exit -> method -> {} ", pjp.getSignature().getName());

		} catch (Throwable t) {
			LOGGER.error("Exception caught in method!! {}", pjp.getSignature().getName());
			throw t;
		}
		return output;
	}

	@AfterThrowing(pointcut = "execution(* com.hackajob.task.phonebookapi.controller.*.*(..))", throwing = "ex")
	public void doRecoveryActions(JoinPoint joinPoint, Throwable ex) {

		Signature signature = joinPoint.getSignature();
		String methodName = signature.getName();
		String exMsg = signature.toString();
		String arguments = Arrays.toString(joinPoint.getArgs());
		LOGGER.error("Exception caught in method!! {}" , methodName);
		LOGGER.error(" with arguments {} ", arguments);
		LOGGER.error(" and exception string is {}", exMsg);
		LOGGER.error("the exception is: {} ", ex.getMessage());

	}
}
