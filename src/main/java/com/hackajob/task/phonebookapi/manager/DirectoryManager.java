package com.hackajob.task.phonebookapi.manager;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.hackajob.task.phonebookapi.dao.DirectoryDAO;
import com.hackajob.task.phonebookapi.entity.Directory;
import com.hackajob.task.phonebookapi.exception.EntryNotFoundException;
import com.hackajob.task.phonebookapi.exception.ValidationException;
import com.hackajob.task.phonebookapi.model.request.DirectoryDTO;

@Component
public class DirectoryManager {

	@Autowired
	private DirectoryDAO directoryDAO;

	/**
	 * 
	 * @param directory
	 * @return
	 */
	public Long addEntry(DirectoryDTO directory) {

		if (directoryDAO.findDirectoryByEmail(directory.getEmail()).isPresent()) {
			throw new ValidationException("User with email id :" + directory.getEmail() + " already exists");
		} else {
			Directory directoryEntity = new Directory();
			directoryEntity.setUserName(directory.getUserName());
			directoryEntity.setEmail(directory.getEmail());
			directoryEntity.setMobileNumber(directory.getMobileNumber());
			directoryEntity.setAlternateNumber(directory.getAlternateNumber());

			Directory savedEntity = directoryDAO.addEntry(directoryEntity);
			return savedEntity.getDirectoryId();
		}
	}

	/**
	 * 
	 * @param id
	 * @param directory
	 */
	public void updateEntry(int id, DirectoryDTO directory) {

		Optional<Directory> entityOpt = directoryDAO.findDirectoryById(Long.valueOf(id));

		if (entityOpt.isPresent()) {
			Directory entity = entityOpt.get();
			if (null != directory.getAlternateNumber()) {
				entity.setAlternateNumber(directory.getAlternateNumber());
			}
			if (null != directory.getEmail()) {
				entity.setEmail(directory.getEmail());
			}
			if (null != directory.getMobileNumber()) {
				entity.setMobileNumber(directory.getMobileNumber());
			}
			directoryDAO.addEntry(entity);

		} else {
			throw new EntryNotFoundException("Entry with id: " + id + " is not found.");
		}

	}

	/**
	 * 
	 * @param id
	 */
	public void deleteEntry(int id) {

		Optional<Directory> toBeDeleted = directoryDAO.findDirectoryById(Long.valueOf(id));
		if (toBeDeleted.isPresent()) {
			directoryDAO.deleteDirectory(toBeDeleted.get());
		} else {
			throw new EntryNotFoundException("Entry with id: " + id + " is not found.");
		}
	}

	/**
	 * 
	 * @param id
	 */
	public void deleteEntryByEmail(String email) {

		Optional<Directory> toBeDeleted = directoryDAO.findDirectoryByEmail(email);
		if (toBeDeleted.isPresent()) {
			directoryDAO.deleteDirectory(toBeDeleted.get());
		} else {
			throw new EntryNotFoundException("Entry with email: " + email + " is not found.");
		}
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public DirectoryDTO findDirectoryById(int id) {

		Optional<Directory> directoryOpt = directoryDAO.findDirectoryById(Long.valueOf(id));
		if (directoryOpt.isPresent()) {
			Directory directory = directoryOpt.get();
			DirectoryDTO directoryDTO = new DirectoryDTO();
			directoryDTO.setAlternateNumber(directory.getAlternateNumber());
			directoryDTO.setEmail(directory.getEmail());
			directoryDTO.setMobileNumber(directory.getMobileNumber());
			directoryDTO.setUserName(directory.getUserName());
			return directoryDTO;

		} else {
			throw new EntryNotFoundException("Entry with id: " + id + " is not found.");
		}
	}
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	public DirectoryDTO findDirectoryByEmail(String email) {

		Optional<Directory> directoryOpt = directoryDAO.findDirectoryByEmail(email);
		if (directoryOpt.isPresent()) {
			Directory directory = directoryOpt.get();
			DirectoryDTO directoryDTO = new DirectoryDTO();
			directoryDTO.setAlternateNumber(directory.getAlternateNumber());
			directoryDTO.setEmail(directory.getEmail());
			directoryDTO.setMobileNumber(directory.getMobileNumber());
			directoryDTO.setUserName(directory.getUserName());
			return directoryDTO;

		} else {
			throw new EntryNotFoundException("Entry with email: " + email + " is not found.");
		}
	}

	/**
	 * 
	 * @return
	 */
	public List<DirectoryDTO> findAllEntries() {

		List<DirectoryDTO> directoryList = new ArrayList<>();
		Iterable<Directory> iterable = directoryDAO.findAll();
		Iterator<Directory> iterator = iterable.iterator();
		while (iterator.hasNext()) {
			Directory directory = iterator.next();
			DirectoryDTO directoryDTO = new DirectoryDTO();
			directoryDTO.setAlternateNumber(directory.getAlternateNumber());
			directoryDTO.setEmail(directory.getEmail());
			directoryDTO.setMobileNumber(directory.getMobileNumber());
			directoryDTO.setUserName(directory.getUserName());
			directoryList.add(directoryDTO);
		}
		return directoryList;
	}
}
