package com.hackajob.task.phonebookapi.manager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.hackajob.task.phonebookapi.dao.LoginDAO;
import com.hackajob.task.phonebookapi.entity.RegisteredUser;
import com.hackajob.task.phonebookapi.exception.AuthenticationFailureException;
import com.hackajob.task.phonebookapi.exception.UserNotFoundException;
import com.hackajob.task.phonebookapi.model.request.UserDTO;

@Component
public class LoginManager {

	@Autowired
	private LoginDAO loginDAO;

	public void registerUser(UserDTO user) {

		RegisteredUser registeredUser = new RegisteredUser();
		registeredUser.setFirstName(user.getFirstName());
		registeredUser.setLastName(user.getLastName());
		registeredUser.setEmail(user.getEmail());
		registeredUser.setPassword(user.getPassword());

		loginDAO.registerUser(registeredUser);
	}

	public boolean authenticateUser(UserDTO user) {

		RegisteredUser registeredUser = loginDAO.findUser(user.getEmail());
		if (null != registeredUser) {
			if (user.getPassword().equalsIgnoreCase(registeredUser.getPassword())) {
				return true;
			} else {
				throw new AuthenticationFailureException("email and password do not match.");
			}
		} else {
			throw new UserNotFoundException("User is not found");
		}

	}
}
