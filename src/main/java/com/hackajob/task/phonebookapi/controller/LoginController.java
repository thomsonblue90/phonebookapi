package com.hackajob.task.phonebookapi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hackajob.task.phonebookapi.model.request.UserDTO;
import com.hackajob.task.phonebookapi.model.response.LoginResponse;
import com.hackajob.task.phonebookapi.model.response.ResponseWrapper;
import com.hackajob.task.phonebookapi.service.LoginService;

@RestController
@RequestMapping("/user")
public class LoginController {

	@Autowired
	private LoginService loginService;

	@PostMapping(value = "/register")
	public ResponseEntity<Object> register(@RequestBody UserDTO user) {

		loginService.registerUser(user);

		ResponseWrapper responseWrapper = new ResponseWrapper();
		responseWrapper.setCode(HttpStatus.CREATED.toString());
		responseWrapper.setStatus("User is registered succesfully.");
		return ResponseEntity.status(201).body(responseWrapper);
	}

	@PutMapping(value = "/login")
	public ResponseEntity<Object> login(@RequestBody UserDTO user) {

		String token = loginService.loginUser(user);

		LoginResponse loginResponse = new LoginResponse();
		loginResponse.setToken(token);
		ResponseWrapper responseWrapper = new ResponseWrapper();
		responseWrapper.setCode(HttpStatus.OK.toString());
		responseWrapper.setStatus("Token is generated succesfully.");
		responseWrapper.setResponse(loginResponse);
		return ResponseEntity.status(200).body(responseWrapper);
	}
}
