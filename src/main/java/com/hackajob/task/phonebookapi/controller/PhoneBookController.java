package com.hackajob.task.phonebookapi.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hackajob.task.phonebookapi.model.request.DirectoryDTO;
import com.hackajob.task.phonebookapi.model.response.AddEntryResponse;
import com.hackajob.task.phonebookapi.model.response.ResponseWrapper;
import com.hackajob.task.phonebookapi.service.DirectoryService;

@RestController
@RequestMapping("/phonebook")
public class PhoneBookController {

	@Autowired
	private DirectoryService directoryService;

	@PostMapping()
	public ResponseEntity<Object> addEntry(@RequestBody DirectoryDTO directory) {

		long id = directoryService.addEntry(directory);

		AddEntryResponse response = new AddEntryResponse();
		response.setDirectoryId(id);
		ResponseWrapper responseWrapper = new ResponseWrapper();
		responseWrapper.setCode(HttpStatus.CREATED.toString());
		responseWrapper.setStatus("Entry is added succesfully.");
		responseWrapper.setResponse(response);
		return ResponseEntity.status(201).body(responseWrapper);
	}

	@PutMapping(value = "/{directoryId}")
	public ResponseEntity<Object> updateEntry(@PathVariable int directoryId, @RequestBody DirectoryDTO directory) {

		directoryService.updateEntry(directoryId, directory);
		ResponseWrapper responseWrapper = new ResponseWrapper();
		responseWrapper.setCode(HttpStatus.OK.toString());
		responseWrapper.setStatus("Entry is updated succesfully.");
		return ResponseEntity.status(200).body(responseWrapper);
	}

	@DeleteMapping(value = "/id/{directoryId}")
	public ResponseEntity<Object> deleteEntryById(@PathVariable int directoryId) {

		directoryService.deleteEntryById(directoryId);

		ResponseWrapper responseWrapper = new ResponseWrapper();
		responseWrapper.setCode(HttpStatus.OK.toString());
		responseWrapper.setStatus("Entry is deleted succesfully.");
		return ResponseEntity.status(200).body(responseWrapper);
	}

	@DeleteMapping(value = "/email/{email}")
	public ResponseEntity<Object> deleteEntryByEmail(@PathVariable String email) {

		directoryService.deleteEntryByEmail(email);
		ResponseWrapper responseWrapper = new ResponseWrapper();
		responseWrapper.setCode(HttpStatus.OK.toString());
		responseWrapper.setStatus("Entry is deleted succesfully.");
		return ResponseEntity.status(200).body(responseWrapper);
	}

	@GetMapping(value = "/")
	public ResponseEntity<Object> getAllEntries() {

		List<DirectoryDTO> directorys = directoryService.getALlEntries();
		ResponseWrapper responseWrapper = new ResponseWrapper();
		responseWrapper.setCode(HttpStatus.OK.toString());
		responseWrapper.setStatus("Entry is added succesfully.");
		responseWrapper.setResponse(directorys);
		return ResponseEntity.status(200).body(responseWrapper);
	}

	@GetMapping(value = "/id/{directoryId}")
	public ResponseEntity<Object> getEntry(@PathVariable int directoryId) {

		DirectoryDTO directory = directoryService.getEntryById(directoryId);
		ResponseWrapper responseWrapper = new ResponseWrapper();
		responseWrapper.setCode(HttpStatus.OK.toString());
		responseWrapper.setStatus("Entry is read succesfully.");
		responseWrapper.setResponse(directory);
		return ResponseEntity.status(200).body(responseWrapper);
	}
	
	@GetMapping(value = "/email/{email}")
	public ResponseEntity<Object> getEntryByEmail(@PathVariable String email) {

		DirectoryDTO directory = directoryService.getEntryByEmail(email);
		ResponseWrapper responseWrapper = new ResponseWrapper();
		responseWrapper.setCode(HttpStatus.OK.toString());
		responseWrapper.setStatus("Entry is read succesfully.");
		responseWrapper.setResponse(directory);
		return ResponseEntity.status(200).body(responseWrapper);
	}
}
