package com.hackajob.task.phonebookapi.repository;

import org.springframework.data.repository.CrudRepository;

import com.hackajob.task.phonebookapi.entity.RegisteredUser;

public interface UserRepository extends CrudRepository<RegisteredUser, Long> {

	public RegisteredUser findByEmail(String email);
}
