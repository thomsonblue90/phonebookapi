package com.hackajob.task.phonebookapi.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.hackajob.task.phonebookapi.entity.Directory;

public interface DirectoryRepository extends CrudRepository<Directory, Long> {
	Optional<Directory> findByEmail(String email);
}
