package com.hackajob.task.phonebookapi.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.hackajob.task.phonebookapi.exception.ValidationException;

@Component
public class AuthenticationInterceptor extends HandlerInterceptorAdapter {

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {

		if (request.getRequestURL().toString().contains("/phonebook")) {
			String authToken = request.getHeader("Authorization");
			if (null != authToken) {
				try {
					if (authToken.matches("^Bearer\\s.+"))
						authToken = authToken.split(" ")[1];
					Algorithm algorithm = Algorithm.HMAC256("secret");
					JWTVerifier verifier = JWT.require(algorithm).withIssuer("phonebook").build();
					verifier.verify(authToken);
				} catch (Exception e) {
					throw new ValidationException("Invalid Authorization Token");
				}
			} else {
				throw new ValidationException("Header \"Authorization\" is missing in the request!!");
			}
		}
		return true;
	}
}
