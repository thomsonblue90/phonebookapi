package com.hackajob.task.phonebookapi.exceptionhandler;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.hackajob.task.phonebookapi.exception.AuthenticationFailureException;
import com.hackajob.task.phonebookapi.exception.EntryNotFoundException;
import com.hackajob.task.phonebookapi.exception.RequestIncompleteException;
import com.hackajob.task.phonebookapi.exception.UserNotFoundException;
import com.hackajob.task.phonebookapi.exception.ValidationException;
import com.hackajob.task.phonebookapi.model.response.ResponseWrapper;

@ControllerAdvice
public class CustomExceptionHandler {

	@ExceptionHandler(RequestIncompleteException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ResponseBody
	public ResponseEntity<ResponseWrapper> handleValidationException(RequestIncompleteException exception) {

		ResponseWrapper response = buildResponse(exception);
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);

	}

	@ExceptionHandler(AuthenticationFailureException.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ResponseBody
	public ResponseEntity<ResponseWrapper> handleValidationException(AuthenticationFailureException exception) {

		ResponseWrapper response = buildResponse(exception);
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);

	}

	@ExceptionHandler(UserNotFoundException.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ResponseBody
	public ResponseEntity<ResponseWrapper> handleValidationException(UserNotFoundException exception) {

		ResponseWrapper response = buildResponse(exception);
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);

	}

	@ExceptionHandler(ValidationException.class)
	@ResponseStatus(HttpStatus.UNAUTHORIZED)
	@ResponseBody
	public ResponseEntity<ResponseWrapper> handleValidationException(ValidationException exception) {

		ResponseWrapper response = buildResponse(exception);
		return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(response);

	}

	@ExceptionHandler(EntryNotFoundException.class)
	@ResponseStatus(HttpStatus.UNAUTHORIZED)
	@ResponseBody
	public ResponseEntity<ResponseWrapper> handleValidationException(EntryNotFoundException exception) {

		ResponseWrapper response = buildResponse(exception);
		return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(response);

	}

	private ResponseWrapper buildResponse(Throwable t) {

		ResponseWrapper response = new ResponseWrapper();
		response.setCode(t.getClass().getSimpleName());
		response.setStatus(t.getMessage());
		return response;
	}
}
