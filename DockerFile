FROM openjdk:8-jdk-alpine
VOLUME /tmp
ENV MY_HOME "D:\personal-workspace\PhonebookAPI"
WORKDIR $MY_HOME
ADD /target/phonebook.jar phonebook.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","phonebook.jar"]