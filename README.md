# PhoneBookAPI

APIs for CRUD operation in a PhoneBook Directory  

Application Execution:  
  
   mvn spring-boot:run  
     
Technology Stack:  
    
1. Java 1.8  
2. Spring Boot 2.1 - web, aspect, jpa  
3. Postgres DB  - port: 5432, database: phonebook, username: postgres, password: root
4. Swagger - API Documentation  
5. Sonar Qube - Code Quality Check  
6. JUnit, Mockito - Unit and Integration Tests  
7. Docker  
  
Swagger UI - http://localhost:8080/swagger-ui.html#/  
  
  
Available APIs  
  
1. Register New User  
   - POST http://localhost:8080/user/register  
2. Login  
   - returns JWT token  
   - PUT http://localhost:8080/user/login  
3. Add new entry to PhoneBook  
   - returns ID  
   - POST http://localhost:8080/phonebook  
4. Update entry in PhoneBook  
   - PUT http://localhost:8080/phonebook/4  
5. Delete entry in Phonebook by ID  
   - DELETE http://localhost:8080/phonebook/id/2  
6. Delete entry in Phonebook by Email  
   - DELETE http://localhost:8080/phonebook/email/test@gmail.com  
7. Get entry in Phonebook by ID  
   - GET http://localhost:8080/phonebook/id/2  
8. Get entry in Phonebook by Email  
   - GET http://localhost:8080/phonebook/email/test@gmail.com  
9. Get all entries  
   - GET http://localhost:8080/phonebook/  
